package br.com.start.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.start.model.Classes;
import br.com.start.model.Personagem;
import br.com.start.model.Racas;

public interface PersonagemRepository extends JpaRepository<Personagem, Integer>{
	
	List<Personagem> findAllByClasse(Classes classe);
	List<Personagem> findAllByRaca(Racas classe);
}
