package br.com.start.config;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import br.com.start.exception.ErroDeNegocioException;
import br.com.start.model.dto.ErroDto;

@RestControllerAdvice
public class ErroController {

	@Autowired
	private MessageSource messageSource;

	@ResponseStatus(code =  HttpStatus.BAD_REQUEST)
	@ExceptionHandler(BindException.class)
	@ResponseBody
	public ErroDto handle(BindException exception) {
		List<String> validacoes = new ArrayList<>();
		
		for (FieldError fieldError : exception.getBindingResult().getFieldErrors()) {
			String mensagem = messageSource.getMessage(fieldError, LocaleContextHolder.getLocale());
			validacoes.add(fieldError.getField()+ " : " + mensagem);
			
		}
		
		ErroDto erroDto = new ErroDto();
		erroDto.setErro("Erro de validação");
		erroDto.setValidacoes(validacoes);
		
		return erroDto;
	}
	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseBody
	public ErroDto handle(ConstraintViolationException exception) {
		List<String> validacoes = new ArrayList<>();
		
		for (ConstraintViolation<?> violation : exception.getConstraintViolations()) {
			String path = ((PathImpl) violation.getPropertyPath()).getLeafNode().getName();
			validacoes.add(path + ": " + violation.getMessage());
		}
		
		ErroDto erroDto = new ErroDto();
		erroDto.setErro("Erro de validação");
		erroDto.setValidacoes(validacoes);
		
		return erroDto;
	}
	
	@ExceptionHandler(ErroDeNegocioException.class)
	@ResponseBody
	public ResponseEntity<ErroDto> handle(ErroDeNegocioException exception){
		ErroDto erroDto = new ErroDto();
		erroDto.setErro(exception.getErro());
		
		return ResponseEntity.status(exception.getHttpStatus()).body(erroDto);
		
	}
}
