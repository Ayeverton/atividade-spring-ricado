package br.com.start.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.start.model.Classes;
import br.com.start.model.Racas;
import br.com.start.model.dto.PersonagemEntradaDto;
import br.com.start.model.dto.PersonagemSaidaDto;
import br.com.start.service.PersonagemService;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("personagem")
@Log4j2
@Validated
public class PersonagemController {
	
	@Autowired
	private PersonagemService service;
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public PersonagemSaidaDto salvar(@Valid @RequestBody PersonagemEntradaDto entradaDto) {
		
		log.info("Salvar : {}", entradaDto);
		
		return service.salvar(entradaDto);
	}
	
	@PutMapping("id/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void alterar(@PathVariable("id") Integer id, @Valid @RequestBody PersonagemEntradaDto entradaDto) {
		log.info("alterar:{}, {}", id, entradaDto);
		
		service.alterar(id, entradaDto);
	}
	
	@GetMapping("id/{id}")
	public PersonagemSaidaDto pagarUm(@PathVariable("id") Integer id) {
		log.info("pagarUm: {}", id);
		
		return service.pegarUm(id);
	}
	
	@DeleteMapping("id/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void excluir(@PathVariable("id") Integer id) {
		log.info("excluir: {}", id);		
		service.excluir(id);
	}
	
	@GetMapping
	public List<PersonagemSaidaDto> listar() {
		log.info("listar");
		
		return service.listar();
	}
	
	@GetMapping("classe/{classe}")
	public List<PersonagemSaidaDto> listarClasse(
			@PathVariable("classe")
			Classes classe) {
		log.info("listar Titulo: {}", classe);
		
		return service.listarClasse(classe);
	}
	
	@GetMapping("raca/{raca}")
	public List<PersonagemSaidaDto> listarRaca(
			@PathVariable("raca")
			Racas raca) {
		log.info("listar Titulo: {}", raca);
		
		return service.listarRaca(raca);
	}
}
