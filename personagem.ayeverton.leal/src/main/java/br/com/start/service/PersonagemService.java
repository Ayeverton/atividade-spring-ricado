package br.com.start.service;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.start.exception.ErroDeNegocioException;
import br.com.start.model.Classes;
import br.com.start.model.Personagem;
import br.com.start.model.Racas;
import br.com.start.model.dto.PersonagemEntradaDto;
import br.com.start.model.dto.PersonagemSaidaDto;
import br.com.start.repository.PersonagemRepository;

@Service
public class PersonagemService {

	@Autowired
	private PersonagemRepository repository;
	
	@Autowired
	private ModelMapper mapper;
	
	public PersonagemSaidaDto salvar(PersonagemEntradaDto entradaDto) {
		
		Personagem personagem = new Personagem();
		
		personagem = mapper.map(entradaDto, Personagem.class);
		personagem.montarPersonagem();
		
		Personagem resBanco = repository.save(personagem);
		
		PersonagemSaidaDto saidaDto = mapper.map(resBanco, PersonagemSaidaDto.class);
		
		return saidaDto;
	}
	
	public void alterar(Integer id, PersonagemEntradaDto entradaDto) {
		
		Optional<Personagem> optional = repository.findById(id);
		
		if(optional.isEmpty()) {
			throw new ErroDeNegocioException(HttpStatus.NOT_FOUND, "Impossível alterar, personagem inexistente");
		}
		
		Personagem personagem = optional.get();
		
		mapper.map(entradaDto, personagem);
		personagem.montarPersonagem();
		
		repository.save(personagem);
		
	}

	public PersonagemSaidaDto pegarUm(Integer id) {
		
		Optional<Personagem> optional = repository.findById(id);
		
		if(optional.isEmpty()) {
			throw new ErroDeNegocioException(HttpStatus.NOT_FOUND, "Personagem inexistente");
		}
		
		Personagem personagem = optional.get();
		
		PersonagemSaidaDto saidaDto = new PersonagemSaidaDto();
		
		mapper.map(personagem, saidaDto);
		
		return saidaDto;
	}
	
	public void excluir(Integer id) {
		Optional<Personagem> optional = repository.findById(id);
		
		if(optional.isEmpty()) {
			throw new ErroDeNegocioException(HttpStatus.NOT_FOUND, "Impossível excluir, personagem inexistente");
		}
		
		repository.deleteById(id);
	}
	
	public List<PersonagemSaidaDto> listar(){
		List<Personagem> personagens = repository.findAll();
		
		List<PersonagemSaidaDto> saidaDtos= mapper.map(personagens, new TypeToken<List<PersonagemSaidaDto>>() {}.getType());

		return saidaDtos;
	}
	
	public List<PersonagemSaidaDto> listarClasse(Classes classe){
		
		List<Personagem> personagens = repository.findAllByClasse(classe);
		
		List<PersonagemSaidaDto> saidaDtos= mapper.map(personagens, new TypeToken<List<PersonagemSaidaDto>>() {}.getType());

		return saidaDtos;
		
	}
	
	public List<PersonagemSaidaDto> listarRaca(Racas raca){

		List<Personagem> personagens = repository.findAllByRaca(raca);
		
		List<PersonagemSaidaDto> saidaDtos= mapper.map(personagens, new TypeToken<List<PersonagemSaidaDto>>() {}.getType());

		return saidaDtos;
	}
}
