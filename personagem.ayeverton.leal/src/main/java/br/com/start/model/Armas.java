package br.com.start.model;

import java.util.ArrayList;
import java.util.List;

public enum Armas {
	
	ESPADA_DUAS_MAOS {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 0;
			mana = 0;
			ataque = 20;
			defesa = -5;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, ESPADA {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 0;
			mana = 0;
			ataque = 10;
			defesa = 0;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, ESPADA_E_ESCUDO {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 10;
			mana = 0;
			ataque = 10;
			defesa = 10;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	},
	MACHADO_DUAS_MAOS {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 0;
			mana = 0;
			ataque = 30;
			defesa = -10;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, MACHADO {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 0;
			mana = 0;
			ataque = 15;
			defesa = 0;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, MACHADO_E_ESCUDO {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 10;
			mana = 0;
			ataque = 15;
			defesa = 10;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	},
	ADAGA {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 0;
			mana = 0;
			ataque = 7;
			defesa = 0;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, CLAVA {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 0;
			mana = 0;
			ataque = 20;
			defesa = 0;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, FOICE {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 0;
			mana = 0;
			ataque = 12;
			defesa = 0;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, BESTA {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 0;
			mana = 0;
			ataque = 35;
			defesa = -15;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, LIVRO_DE_MAGIA {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 0;
			mana = 0;
			ataque = 30;
			defesa = -10;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	},
	ARCO {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 0;
			mana = 0;
			ataque = 30;
			defesa = -5;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, LANCA {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 0;
			mana = 0;
			ataque = 23;
			defesa = 0;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, CAJADO {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 0;
			mana = 15;
			ataque = 20;
			defesa = 0;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, ARPA {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 20;
			mana = 50;
			ataque = 0;
			defesa = 0;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, ALAUDE {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 50;
			mana = 100;
			ataque = 2;
			defesa = 0;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, GARRA {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 0;
			mana = 0;
			ataque = 10;
			defesa = 0;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, 
	MARTELO_DE_BATALHA {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 0;
			mana = 0;
			ataque = 30;
			defesa = -10;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, CETRO {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 15;
			mana = 15;
			ataque = 15;
			defesa = 0;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, VARINHA {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 0;
			mana = 25;
			ataque = 15;
			defesa = 0;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	};
	
	private static Integer vida = 0;
	private static Integer mana = 0;
	private static Integer ataque = 0;
	private static Integer defesa = 0;
	
	public abstract List<Integer> setarAtributos();
	
	private static List<Integer> replaceAtributos (int vida, int mana, int ataque, int defesa) {
		
		List<Integer> atributos = new ArrayList<>();
		
		atributos.add(vida);
		atributos.add(mana);
		atributos.add(ataque);
		atributos.add(defesa);
		
		return atributos;
	}
	
}
