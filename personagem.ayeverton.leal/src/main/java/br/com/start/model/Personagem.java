package br.com.start.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity(name = "personagem")
public class Personagem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String nome;
	private Racas raca;
	private Classes classe;
	private Armas arma;
	private Integer vida;
	private Integer mana;
	private Integer ataque;
	private Integer defesa;
	
	public void montarPersonagem() {
		Armas arma = this.arma;
		Classes classe = this.classe;
		Racas raca = this.raca;
		
		if(classe.equals(Classes.MAGO)) {
			System.out.println("Entrou");
			this.nome.concat(nome + ", O mago é implacavel!");
		}
		this.vida = arma.setarAtributos().get(0) + classe.setarAtributos().get(0) + raca.setarAtributos().get(0);
		this.mana = arma.setarAtributos().get(1) + classe.setarAtributos().get(1) + raca.setarAtributos().get(1);
		this.ataque = arma.setarAtributos().get(2) + classe.setarAtributos().get(2) + raca.setarAtributos().get(2);
		this.defesa = arma.setarAtributos().get(3) + classe.setarAtributos().get(3) + raca.setarAtributos().get(3);
		
		vida = atributoMaximo(vida);
		mana = atributoMaximo(mana);
		ataque = atributoMaximo(ataque);
		defesa = atributoMaximo(defesa);

	}
	
	private static Integer atributoMaximo(Integer i) {
		
		if(i>9999) return 9999;
		
		return i;
	}
	
}
