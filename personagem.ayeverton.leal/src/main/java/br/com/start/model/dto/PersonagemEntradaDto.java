package br.com.start.model.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.start.model.Armas;
import br.com.start.model.Classes;
import br.com.start.model.Racas;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PersonagemEntradaDto {

	@NotBlank(message = "Campo obrigatório")
	@Size(min = 6, max = 16, message = "O nome deve ter entre 6 e 16 caracteres")
	private String nome;
	
	@NotNull(message = "Campo obrigatório")
	private Racas raca;
	
	@NotNull(message = "Campo obrigatório")
	private Classes classe;
	
	@NotNull(message = "Campo obrigatório")
	private Armas arma;
	
}
