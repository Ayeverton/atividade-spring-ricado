package br.com.start.model;

import java.util.ArrayList;
import java.util.List;

public enum Classes {
	PALADINO {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 100;
			mana = 100;
			ataque = 50;
			defesa = 150;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, LADINO {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 50;
			mana = 50;
			ataque = 150;
			defesa = 30;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, BRUXO {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 30;
			mana = 300;
			ataque = 250;
			defesa = 0;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, 
	FEITICEIRO {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 50;
			mana = 100;
			ataque = 350;
			defesa = 40;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, MAGO {
		@Override
		public List<Integer> setarAtributos() {
			//o mago é implacavel
			
			vida = 9999;
			mana = 9999;
			ataque = 9999;
			defesa = 9999;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, ARQUEIRO {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 100;
			mana = 40;
			ataque = 350;
			defesa = 20;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, 
	BARDO {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 50;
			mana = 200;
			ataque = 2;
			defesa = 20;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, MONGE {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 200;
			mana = 200;
			ataque = 250;
			defesa = 100;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, DRUID {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 250;
			mana = 150;
			ataque = 100;
			defesa = 75;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, CACADOR {
		@Override
		public List<Integer> setarAtributos() {
			
			vida = 200;
			mana = 50;
			ataque = 150;
			defesa = 120;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	};
	
	private static Integer vida = 0;
	private static Integer mana = 0;
	private static Integer ataque = 0;
	private static Integer defesa = 0;
	
	public abstract List<Integer> setarAtributos();
	
	private static List<Integer> replaceAtributos (int vida, int mana, int ataque, int defesa) {
		
		List<Integer> atributos = new ArrayList<>();
		
		atributos.add(vida);
		atributos.add(mana);
		atributos.add(ataque);
		atributos.add(defesa);
		
		return atributos;
	}
}
