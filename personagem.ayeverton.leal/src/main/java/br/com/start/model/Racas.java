package br.com.start.model;

import java.util.ArrayList;
import java.util.List;

public enum Racas {
	HUMANO {
		@Override
		public List<Integer> setarAtributos() {
			vida = 200;
			mana = 75;
			ataque = 100;
			defesa = 100;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, ANAO {
		@Override
		public List<Integer> setarAtributos() {
			vida = 300;
			mana = 75;
			ataque = 150;
			defesa = 75;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, ORC {
		@Override
		public List<Integer> setarAtributos() {
			vida = 300;
			mana = 25;
			ataque = 175;
			defesa = 150;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, ELFO {
		@Override
		public List<Integer> setarAtributos() {
			vida = 250;
			mana = 150;
			ataque = 125;
			defesa = 50;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, ELFO_NEGRO {
		@Override
		public List<Integer> setarAtributos() {
			vida = 175;
			mana = 150;
			ataque = 150;
			defesa = 75;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	}, LIZARDMAN {
		@Override
		public List<Integer> setarAtributos() {
			vida = 500;
			mana = 0;
			ataque = 150;
			defesa = 130;
			
			return replaceAtributos(vida, mana, ataque, defesa);
		}
	};
	
	private static Integer vida = 0;
	private static Integer mana = 0;
	private static Integer ataque = 0;
	private static Integer defesa = 0;
	
	public abstract List<Integer> setarAtributos();
	
	private static List<Integer> replaceAtributos (int vida, int mana, int ataque, int defesa) {
		
		List<Integer> atributos = new ArrayList<>();
		
		atributos.add(vida);
		atributos.add(mana);
		atributos.add(ataque);
		atributos.add(defesa);
		
		return atributos;
	}
}
