package br.com.start.model.dto;

import br.com.start.model.Armas;
import br.com.start.model.Classes;
import br.com.start.model.Racas;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PersonagemSaidaDto {

	private Integer id;
	private String nome;
	private Racas raca;
	private Classes classe;
	private Armas arma;
	private Integer vida;
	private Integer mana;
	private Integer ataque;
	private Integer defesa;
}
